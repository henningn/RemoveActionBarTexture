local function HideArt(frame)
    local y = { frame:GetRegions() }
    for k, v in pairs(y) do
        if v:GetObjectType() == "Texture" then
            v:SetTexture(nil)
        end
    end
    y = { frame:GetChildren() }
    for k, v in pairs(y) do
        if v:GetObjectType() ~= "CheckButton" and v:GetObjectType() ~= "Button" then
            HideArt(v)
        end
    end
end
HideArt(MainMenuBarArtFrame)
HideArt(MicroButtonAndBagsBar)
